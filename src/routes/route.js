import HomePage from '../page/HomePage'
import Index from '../page/Index'
import NewsDetail from '../page/NewsDetail'
import React from 'react'
const route=[
    {
        path: '/',
        exact: true,
        main:()=><HomePage/>
    },
    {
        path:'/result/news',
        exact:false,
        main:()=><Index/>
    },
    {
        path:'/modal',
        exact:false,
        main:()=><NewsDetail/>
    }
]
export default route;