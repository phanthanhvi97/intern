import './Router.css';
import React, { Component } from 'react'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import route from '../routes/route'
export default class App extends Component {
  render() {
    return (
      <Router>
        <div className='container-fluid'>
          <Switch>
            {this.showComponent(route)}
          </Switch>
        </div>
      </Router>
    )
  }
  showComponent=route=>{
    var result=null
    if(route.length>0){
      result=route.map((route, index)=>{
        return (
          <Route 
          key={index}
          path={route.path} 
          exact={route.exact}
          component={route.main}></Route>
        )
      })
    }
    return result
  }
}

