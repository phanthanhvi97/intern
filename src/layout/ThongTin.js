import React, { Component } from 'react'
import { connect } from 'react-redux'
import { actFetchPostByKeywordRequest, actCountByKeywordRequest } from '../actions/index'

class ThongTin extends Component {
    onClick = () => {
        this.props.fetchPostRequest('P&D KOREA')
        this.props.countByKeywordRequest('P&D KOREA')
    }
    render() {
        return (
            <React.Fragment>
                <div className="ht-menu--item"><a href="/result" onClick={this.onClick}>Th&ocirc;ng tin</a></div>
                <div className="ht-menu--item"><a href="/ban-do">Bản đồ</a></div>
                <div className="ht-menu--item"><a href="/ket-qua/bang-gia">B&#x1EA3;ng gi&aacute;</a></div>
                <div className="ht-menu--item dropdown">
                    <div className="dropdown-toggle" data-toggle="dropdown">Th&ecirc;m</div>
                    <div className="dropdown-menu ht-menu__add">
                        <div className="dropdown-item"><a href="#">Danh b&#x1EA1;</a></div>
                        <div className="dropdown-item"><a href="#">T&agrave;i nguy&ecirc;n</a></div>
                        <div className="dropdown-item"><a href="#">Hỏi đáp</a></div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
const mapStateToProps = (state) => {
    return {
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchPostRequest: keyword => {
            dispatch(actFetchPostByKeywordRequest(keyword))
        },
        countByKeywordRequest: keyword => {
            dispatch(actCountByKeywordRequest(keyword))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ThongTin);