import React, { Component } from 'react'
import '../assets/stylesheets/pages/_resultpages.css'
import '../assets/stylesheets/style.css'
import { connect } from 'react-redux'
import { actFetchPostByKeywordRequest, actCountByKeywordRequest } from '../actions/index'
import {Link} from 'react-router-dom'
class ItemFound extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isSelected:false,
            index:'',

            itemSelected:-1
        }
    }
    componentWillReceiveProps(e){
        this.setState({itemSelected:e.itemSelected, index:e.index})
    }
    onMouseOver = () => {
        this.props.mouseSelectedItem(this.state.index)
    }
    onMouseOut = () => {
        this.props.mouseSelectedItem(-1)
    }
    onClick = (e) => {
        this.props.countByKeywordRequest(this.props.item.title)
        this.props.fetchPostRequest(this.props.item.title,1)
        this.props.setIsFocus(false)
    }
    render() {
        var { item } = this.props
        return (
            <Link to={`/result/news?page=1&keyword=${this.props.title}`} style={{ textDecoration: 'none', color: 'black' }}>
                <li
                    onClick={this.onClick}
                    onMouseOver={this.onMouseOver}
                    onMouseOut={this.onMouseOut}
                    title={item.title}
                    className={this.state.index===this.state.itemSelected||this.state.isSelected? "row-item-suggestion-popup pointer active-suggestion" : "row-item-suggestion-popup pointer"}>
                    <div style={{ textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden' }}>
                        {item.title}
                    </div>
                </li>
            </Link>
        )
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchPostRequest: (keyword,page) => {
            dispatch(actFetchPostByKeywordRequest(keyword, page))
        },
        countByKeywordRequest:keyword=>{
            dispatch(actCountByKeywordRequest(keyword))
        }
    }
}
export default connect(null, mapDispatchToProps)(ItemFound);