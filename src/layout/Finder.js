import React, { Component } from 'react'
import { actFetchSuggestRequest, actFetchPostByKeywordRequest, actCountByKeywordRequest } from '../actions/index'
import { connect } from 'react-redux'
import '../assets/stylesheets/pages/_resultpages.css'
import '../assets/stylesheets/style.css'
import ItemFound from './ItemFound'
import { withRouter } from 'react-router-dom';
import queryString from 'query-string'

class Finder extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isExpanded: false,
            isFocus: false,

            itemSelected: -1,
            value: 'hi',
            empty: ' ',
            listSuggest: []
        }
    }
    componentWillReceiveProps(e) {
        this.setState({
            listSuggest: e.suggests
        })
    }
    componentWillMount() {
        const { location } = this.props;
        let url = location.search
        const parsed = queryString.parse(url);
        this.setState({
            value: parsed.keyword
        })
    }
    onChange = e => {
        this.setState({
            value: e.target.value
        }, () => {
            this.props.fetchSuggestRequest(this.state.value)
        })
        if (e.target.value !== '') {
            this.setState({
                isExpanded: true
            })
        }
        else {
            this.setState({
                isExpanded: false
            })
        }
    }
    handleKey = (e) => {
        const { listSuggest, itemSelected, empty } = this.state
        const { length } = listSuggest
        if (e.keyCode === 40) { //xuong
            if (itemSelected >= length - 1) {
                this.setState({
                    itemSelected: -1,
                    value: empty,
                });
            } else {
                this.setState({
                    itemSelected: itemSelected + 1,
                    value: listSuggest[itemSelected + 1].title
                })
            }
        }
        if (e.keyCode === 38 && listSuggest[length - 1]) { //len
            if (itemSelected <= -1) {
                this.setState({
                    itemSelected: length - 1,
                    value: listSuggest[length - 1].title,
                });
            } else {
                this.setState({
                    itemSelected: itemSelected - 1,
                    value: itemSelected - 1 <= -1 ? empty : listSuggest[itemSelected - 1].title
                })
            }
            e.preventDefault();
        }
        if (e.keyCode === 13) {
            this.props.fetchPostRequest(this.state.value, 1)
            this.props.countByKeywordRequest(this.state.value)
            this.props.history.push(`/result/news?page=1&keyword=${this.state.value}`)
            this.setState({ isFocus: false })
        }
    }
    focus = () => {
        this.setState({ isFocus: true })
    }
    onBlur = (e) => {
        if (!e.relatedTarget) {
            this.setState({ isFocus: false })
        }
    }
    setIsFocus = () => {
        this.setState({ isFocus: false })
    }
    mouseSelectedItem = (index) => {
        this.setState({ itemSelected: index })
    }
    onFocus
    render() {
        var listSuggest = this.props.suggests
        return (
            <React.Fragment>
                <div className={this.state.isExpanded && this.state.isFocus ? "w-100 input-search__content expanded" : "w-100 input-search__content collapsed"} id="autoComplete__content">
                    <input
                        className="form-control"
                        id="autoComplete"
                        name="keyword"
                        type="text"
                        placeholder="Nhập từ khóa ..."
                        onChange={this.onChange}
                        value={this.state.value || ''}
                        onFocus={this.focus}
                        onBlur={this.onBlur}
                        autoComplete="off"
                        onKeyDown={this.handleKey}
                        ref={input => { this.inputTag = input }}
                    />
                    {listSuggest.length >= 1 ?
                        <ul id="autoComplete_results_list">
                            {
                                listSuggest.map((item, index) => <ItemFound
                                    index={index}
                                    key={index}
                                    item={item}
                                    title={item.title}
                                    itemSelected={this.state.itemSelected}
                                    setIsFocus={this.setIsFocus}
                                    mouseSelectedItem={this.mouseSelectedItem}
                                />)
                            }
                        </ul> : ""}
                </div>

            </React.Fragment>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        suggests: state.Searching,
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchSuggestRequest: keyword => {
            dispatch(actFetchSuggestRequest(keyword))
        },
        fetchPostRequest: (keyword, page) => {
            dispatch(actFetchPostByKeywordRequest(keyword, page))
        },
        countByKeywordRequest: keyword => {
            dispatch(actCountByKeywordRequest(keyword))
        }
    }
}
export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(Finder)
)