import React, { Component } from 'react'

export default class Login extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="ht-menu--item ht-menu--item__tools ht-menu-tools dropdown">
                    <div className="menu-tools__btn-toggle dropdown-toggle" data-toggle="dropdown"><img className="ht-menu--icon" src="../../../assets/img/Group 1.png" alt="" /></div>
                    <div className="menu-tools__dropdown-menu dropdown-menu">
                        <div className="mt--dropdown-item dropdown-item"><a href="#"><img src="../../../assets/img/Group 1569.png" alt="" /><span className="ht-menu--text">T&iacute;nh to&aacute;n kho&#x1EA3;n vay</span></a></div>
                        <div className="mt--dropdown-item dropdown-item"><a href="#"><img src="../../../assets/img/baseline-insert_chart-24px.png" alt="" /><span className="ht-menu--text">
                            T&iacute;nh to&aacute;n hi&#x1EC7;u qu&#x1EA3; d&#x1EF1; &aacute;n</span></a>
                        </div>
                        <div className="mt--dropdown-item dropdown-item"><a href="#"><img src="../../../assets/img/baseline-monetization_on-24px.png" alt="" /><span className="ht-menu--text">T&agrave;i ch&iacute;nh c&aacute; nh&acirc;n</span></a></div>
                    </div>
                </div>
                <div className="ht-menu--item">
                    <div className="ht__menu-notify"><a href="#"><img className="ht-menu--icon" src="../../assets/img/Group 22.png" alt="" /></a></div>
                </div>
                <div className="ht-menu--item"><a className="ht-menu__login menu-tools--btn-login btn btn-primary" href="/dang-nhap">Đăng nhập</a></div>
            </React.Fragment>
        )
    }
}
