import React, { Component } from 'react'

export default class MenuFooter extends Component {
    render() {
        return (
            <div>
                <div className="hp-footer__links layout-footer__links d-flex justify-content-center justify-content-lg-start">
                    <div className="hp-footer--link layout-footer--link"><a href="#">Gi&#x1EDB;i thi&#x1EC7;u</a></div>
                    <div className="hp-footer--link layout-footer--link"><a href="#">H&#x1B0;&#x1EDB;ng d&#x1EAB;n</a></div>
                </div>
            </div>
        )
    }
}
