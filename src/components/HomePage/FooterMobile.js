import React, { Component } from 'react'

export default class FooterMobile extends Component {
    render() {
        return (
            <div className="hp-footer__quick-views layout-footer__quick-views d-flex d-lg-none">
                <div className="hp-footer__quick-view layout-footer--quick-view">
                    <marquee>
                        <div className="quick-view__content d-inline-flex"><span><span className="quick-view--title">Tin nhanh 1: </span> Lorem Ipsum. Proin gravida nibh vel velit
                                                auctor aliquet. Aenean
                                          sollicitudin, lorem quis biben <a href="#">Xem nhanh</a></span></div>
                        <div className="quick-view__content d-inline-flex"><span><span className="quick-view--title">Tin nhanh 2: </span> Lorem Ipsum. Proin gravida nibh vel velit
                                                auctor aliquet. Aenean
                                          sollicitudin, lorem quis biben <a href="#">Xem nhanh</a></span></div>
                    </marquee>
                </div>
            </div>
        )
    }
}
