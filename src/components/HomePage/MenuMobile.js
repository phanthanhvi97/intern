import React, { Component } from 'react'

export default class MenuMobile extends Component {
    render() {
        return (
            <div className="menu_mobile__items d-flex align-items-center">
                <div className="menu_mobile--item dropdown dropdown__menu-tool">
                    <div className="dropdown-toggle d-flex" data-toggle="dropdown"><img className="img_menu" src="../../assets/img/Group 1.png" alt="" /></div>
                    <div className="dropdown-menu menu-tool">
                        <div className="dropdown-item"><a href="#"><img src="../../assets/img/Group 1569.png" alt="" /><span className="menu-tool--text">
                            T&iacute;nh to&aacute;n kho&#x1EA3;n
                          vay</span></a></div>
                        <div className="dropdown-item"><a href="#"><img src="../../assets/img/baseline-insert_chart-24px.png" alt="" /><span className="menu-tool--text">T&iacute;nh to&aacute;n hi&#x1EC7;u qu&#x1EA3; d&#x1EF1; &aacute;n</span></a></div>
                        <div className="dropdown-item"><a href="#"><img src="../../assets/img/baseline-monetization_on-24px.png" alt="" /><span className="menu-tool--text">T&agrave;i ch&iacute;nh c&aacute; nh&acirc;n</span></a></div>
                    </div>
                </div>
                <div className="menu_mobile--item">
                    <div className="menu-notify"><a className="d-flex" href="#"><img className="img_menu" src="../../assets/img/Group 22.png" alt="" /></a></div>
                </div>
                <div className="menu_mobile--item"><a className="btn-login" href="/dang-nhap"><img src="../../assets/img/icon-avatar.png" alt="" /></a></div>
            </div>
        )
    }
}
