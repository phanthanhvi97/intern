import React, { Component } from 'react'

export default class NavbarItem extends Component {
    render() {
        return (
            <div className="navbar__items">
                <div className="navbar--item"><a href="/ket-qua/thong-tin">Th&ocirc;ng tin</a></div>
                <div className="navbar--item"><a href="/ban-do">Bản đồ</a></div>
                <div className="navbar--item"><a href="/ket-qua/bang-gia">B&#x1EA3;ng gi&aacute;</a></div>
                <div className="navbar--item"><a href="#">Danh b&#x1EA1;</a></div>
                <div className="navbar--item"><a href="#">T&agrave;i nguy&ecirc;n</a></div>
                <div className="navbar--item"><a href="#">Hỏi đáp</a></div>
            </div>
        )
    }
}
