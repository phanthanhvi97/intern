import React, { Component } from 'react'
import Login from '../../layout/Login'
import ThongTin from '../../layout/ThongTin'
export default class MenuItem extends Component {
    render() {
        return (
            <div className="d-flex align-items-center ht-menu__items">
                <ThongTin/>
                <Login/>
            </div>
        )
    }
}
