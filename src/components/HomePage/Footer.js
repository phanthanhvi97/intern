import React, { Component } from 'react'

export default class Footer extends Component {
    render() {
        return (
            <div className="hp-footer__quick-views layout-footer__quick-views d-none d-lg-flex">
                <div className="hp-footer__quick-view layout-footer--quick-view w-50">
                    <marquee>
                        <div className="quick-view__content"><span><span className="quick-view--title">Tin nhanh 1: </span> Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean
                                          sollicitudin, lorem quis biben <a href="#">Xem nhanh</a></span></div>
                    </marquee>
                </div>
            </div>
        )
    }
}
