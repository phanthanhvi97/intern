import React, { Component } from 'react'
import { connect } from 'react-redux'
import { actGetPostByID } from '../../actions/index'
import { withRouter } from 'react-router-dom';
class SearchResult extends Component {
    constructor(props) {
        super(props)
        this.state = {
            day: '',
            avatar:'',
            first_name:'',
            last_name:'',
            create_user_role:'',
            description:'',
            name:'',
            reaction_lenght:''            
        }
    }
    onClick = (e) => {
        this.props.loadDetail(this.props.item._id)
    }
    componentDidMount() {
        const { item } = this.props
        let date_publish = item.publish_date
        let one_day = 1000 * 60 * 60 * 24
        let present_date = new Date()
        let public_date = new Date(present_date.getFullYear(), date_publish.substr(5, 2) - 1, date_publish.substr(8, 2))
        let Result = Math.round(present_date.getTime() - public_date.getTime()) / (one_day)
        let Final_Result = Result.toFixed(0);
        if (Final_Result / 30 >= 1) {
            let a = Final_Result / 30
            let b = a.toFixed(0)
            this.setState({ day: b + " tháng trước" })
        } else (
            this.setState({ day: Final_Result + " ngày trước" })
        )
    }
    render() {
        const { item} = this.props;
        return (
            <React.Fragment>
                <div className="rp-search-result-item">
                    <div className="search-result__item-container d-flex"><img className="sr-item__modal" src="../../../assets/img/icon-modal.png" />
                        <div className="sr-item__poster">
                            <div className="poster--avatar"><a href="#"><img src={item.create_uid.avatar} alt="" width="73px" height="72px" /></a></div>
                        </div>
                        <div className="sr-item__details">
                            <div className="item-details__container">
                                <div className="item-details__title">
                                    <div className="ids-title__content">
                                        <div className="ids-title--header d-flex"><a className="title--name" href="#">{item.create_uid.first_name + '' + ' ' + item.create_uid.last_name}</a>
                                            <div className="title--rate d-flex align-items-center">[<span>{item.create_user_role}</span>]</div>
                                        </div>
                                        <p className="mb-0 ids-title--category">đã đăng một bài viết trong <a className="ids-title--category" href="/ket-qua/thong-tin">Thông tin</a></p>
                                        <p className="mb-0 ids-title--date-posted">
                                            {this.state.day}
                                        </p>
                                    </div>
                                </div>
                                <div className="item-details__post">
                                    <div className="ids-post--title"><a href="#" onClick={this.onClick} data-toggle="modal" data-target="#newsDetailsModal">{item.name}</a></div>
                                    <div className="ids-post--content"><span>
                                        {item.description}</span></div>
                                </div>
                                <div className="item-details__actions">
                                    <div className="action-buttons-bottom">
                                        <a href="#" className="btn -comment">
                                            <span className="fa fa-comment-o icon" /></a>
                                        <a href="#" className="btn -liked">
                                            <span className="-ap  icon-like2 icon" />{item.reaction_lenght}</a>
                                        <a href="#" className="btn -share">
                                            <span className="-ap  icon-share4 icon" /></a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
const mapStateToProps = (state) => {
    return {

    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        loadDetail: data => {
            dispatch(actGetPostByID(data))
        }
    }
}
export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(SearchResult),
);