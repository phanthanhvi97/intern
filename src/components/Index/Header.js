import React, { Component } from 'react'
import Login from '../../layout/Login'
import Finder from '../../layout/Finder'
import {Link} from 'react-router-dom'
class Header extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="result-pages__header-container layout-header__container">
                    <div className="result-pages__header-top layout-header__top">
                        <div className="d-flex align-items-center rp-header-top__container lh-top__container">
                            <div className="rp-header-top__logo lh-top__logo">
                                <div className="header-top--logo lh-top--logo"><Link to="/" ><img src="../../../assets/img/icon-asset.png" alt="" onClick={this.onClick}/></Link></div>
                            </div>
                            <div className="rp-header-top__search-input lh-top__search-input">
                                <div className="search-input">
                                    <Finder/>
                                </div>
                            </div>
                            <div className="ml-auto rp-header-top__menu lh-top__menu">
                                <div className="d-flex align-items-center ht-menu__items">
                                    <Login />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="result-pages__header-navigation">
                        <div className="rp-header-navigation header-navigation__container">
                            <div className="header-navigation__items d-flex">
                                <div className="header-navigation--item"><a className="hn-item--text" href="/ket-qua">Tất cả</a></div>
                                <div className="header-navigation--item is-actived"><a className="hn-item--text" href="/ket-qua/thong-tin">Thông tin</a></div>
                                <div className="header-navigation--item"><a className="hn-item--text" href="/ban-do">Bản đồ</a></div>
                                <div className="header-navigation--item"><a className="hn-item--text" href="/ket-qua/bang-gia">Bảng giá</a></div>
                                <div className="header-navigation--item dropdown">
                                    <div className="hn-item--text dropdown-toggle" data-toggle="dropdown">Thêm <span className="fa icon" /></div>
                                    <div className="dropdown-menu hn-menu__add">
                                        <div className="dropdown-item"><a href="#">Danh bạ</a></div>
                                        <div className="dropdown-item"><a href="#">Tài nguyên</a></div>
                                        <div className="dropdown-item"><a href="#">Hỏi đáp</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export default Header