import * as Types from '../constants/ActionType'
import callApi from '../utils/apiCaller'
//For Suggest Search
export const actFetchSuggestRequest = keyword => {
    return dispatch => {
        return callApi(`api/v1/search/suggestion?keyword=${keyword}`, 'GET', null).then(res => {
            dispatch(actFetchSuggestSuccess(res.data))
        }).catch(err=>{
            dispatch(actFetchSuggestFail(err))
        })
    }
}

export const actFetchSuggestSuccess = data => {
    return {
        type: Types.FETCH_SUGGEST_SUCCESS,
        payload: {
            data
        }
    }
}

export const actFetchSuggestFail = err => {
    return {
        type: Types.FETCH_SUGGEST_FAIL,
        payload: {
            err
        }
    }
}
//load detail
export const actGetPostByID=data=>{
    return dispatch=>{
        return callApi(`api/v1/post/get_by_id?postID=${data}`,'GET',null).then(res=>{
            dispatch(actGetPostByIDSuccess(res.data))
        }).catch(err=>{
            dispatch(actGetPostByIDFail(err))
        })
    }
}
export const actGetPostByIDSuccess=data=>{
    return{
        type:Types.GET_POST_BY_ID_SUCCESS,
        payload:{
            data
        }
    }
}
export const actGetPostByIDFail=err=>{
    return{
        type:Types.GET_POST_BY_ID_FAIL,
        payload:{
            err
        }
    }
}
//lay bai viet sau khi tim kiem bang keyword
export const actFetchPostByKeywordRequest=(keyword, page)=>{
    return dispatch=>{
        // return callApi(`api/v1/search/get_post_by_keyword?keyword=${data}&page=1`,'GET',null).then(res=>{
            return callApi(`api/v1/search/get_post_by_keyword?keyword=${keyword}&page=${page}`,'GET',null).then(res=>{
            dispatch(actFetchPostByKeywordSuccess(res.data))
        }).catch(err=>{
            dispatch(actFetchPostByKeywordFail(err))
        })
    }
}
export const actFetchPostByKeywordSuccess=data=>{
    return{
        type:Types.FETCH_POST_BY_KEYWORD_SUCCESS,
        payload:{
            data
        }
    }
}
export const actFetchPostByKeywordFail=err=>{
    return{
        type:Types.FETCH_POST_BY_KEYWORD_FAIL,
        payload:{
            err
        }
    }
}
//count
export const actCountByKeywordRequest = data=>{
    return dispatch=>{
        return callApi(`api/v1/search/count_by_keyword?keyword=${data}&type=post`,'GET', null).then(res=>{
            dispatch(actCountByKeyword(res.data))
        })
    }
}
export const actCountByKeyword=data=>{
    return{
        type:Types.COUNT_BY_KEYWORD,
        payload:{
            data
        }
    }
}