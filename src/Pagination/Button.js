import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import queryString from 'query-string';
import {withRouter} from 'react-router-dom'
import { connect } from 'react-redux'
import {actFetchPostByKeywordRequest} from '../actions/index'

 class Button extends Component {
    onClick = () => {
        this.props.setPageSelected(this.props.pagenumber)

        const {location, pagenumber} = this.props;
        const {keyword}=queryString.parse(location.search)
        this.props.fetchPostRequest(keyword, pagenumber)
    }
    render() {
        const {location, pagenumber, isactive} = this.props;
        const {keyword}=queryString.parse(location.search)
        return (
            <Link
                className={`sr-pagination--item ${isactive ? 'is-actived' : ''}`}
                to={`${location.pathname}?page=${pagenumber}&keyword=${keyword}`}
                onClick={this.onClick}>
                {pagenumber}
            </Link>
        )
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchPostRequest: (keyword,page) => {
            dispatch(actFetchPostByKeywordRequest(keyword, page))
        }
    }
}
export default withRouter(connect(null, mapDispatchToProps)(Button))
