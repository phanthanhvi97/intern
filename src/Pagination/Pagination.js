import React, { Component } from 'react'
import Button from './Button'
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import queryString from 'query-string'
import { actFetchPostByKeywordRequest } from '../actions/index'
class Pagination extends Component {
    constructor(props) {
        super(props)

        this.state = {
            pageSelected: 1,
            totalItem: 1,
            keyword: '',

        }
    } 
    // componentWillReceiveProps(e) {
    //     this.setState({ totalItem: e.soluong })
    // }
    componentWillMount() {
        const { location } = this.props;
        let url = location.search
        const parsed = queryString.parse(url);
        this.setState({
            pageSelected: +parsed.page,
            keyword: parsed.keyword,
        })
    }
    setPageSelected(page) {
        this.setState({ pageSelected: page });
    }
    back = () => {
        if (this.state.pageSelected > 1) {
            this.setState({ pageSelected: this.state.pageSelected - 1 }
                , () => {
                    this.props.fetchPostRequest(this.state.keyword, this.state.pageSelected)
                }
            )
        }
    }
    next = () => {
        if (this.state.pageSelected < Math.ceil(+this.state.totalItem/10)) {
            this.setState({ pageSelected: this.state.pageSelected + 1 }, () => {
                this.props.fetchPostRequest(this.state.keyword, this.state.pageSelected)
            })
        }
    }
    renderButton() {
        // console.log(this.state)
        const pageTotal = Math.ceil(this.props.soluong / 10);
        const listbtn = []
        let sl = 1
        while (pageTotal - sl >= 0) {
            listbtn.push(sl)
            sl++
        }
        return listbtn.map(number =>
            <Button
                key={number}
                pagenumber={number}
                isactive={number === this.state.pageSelected}
                setPageSelected={(page) => this.setPageSelected(page)}
            />
        )
    }
    render() {
        console.log(this.props.soluong)
        const { location } = this.props;
        const { keyword } = queryString.parse(location.search)
        return (
            <div className="rp-search-result__pagination">
                <div className="search-result__pagination-container container">
                    <div className="search-result__pagination-content d-flex align-items-center justify-content-center justify-content-sm-end">
                        <div className="sr-pagination__items d-flex align-items-center">
                            <Link to={`${location.pathname}?page=${this.state.pageSelected}&keyword=${keyword}`} className="sr-pagination--btn sr-pagination--previous" onClick={this.back}>Trước</Link>
                            {this.renderButton()}
                            <Link to={`${location.pathname}?page=${this.state.pageSelected}&keyword=${keyword}`} className="sr-pagination--btn sr-pagination--next" onClick={this.next}>Tiếp</Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        soluong:state.Keyword
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchPostRequest: (keyword, page) => {
            dispatch(actFetchPostByKeywordRequest(keyword, page))
        }
    }
}
export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(Pagination)
)