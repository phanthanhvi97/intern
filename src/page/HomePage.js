import React, { Component } from 'react'
import '../assets/stylesheets/index.css'
import '../assets/stylesheets/pages/_home.css'
// import Footer from '../components/HomePage/Footer'
import FooterMobile from '../components/HomePage/FooterMobile'
import HomePageBody from '../components/HomePage/HomePageBody'
import MenuFooter from '../components/HomePage/MenuFooter'
import MenuItem from '../components/HomePage/MenuItem'
import MenuMobile from '../components/HomePage/MenuMobile'
import NavbarItem from '../components/HomePage/NavbarItem'
import '../vendor/ap8/css/style.css'
import '../vendor/bootstrap/css/bootstrap.min.css'
import '../vendor/font-awesome/css/font-awesome.min.css'
export default class HomePage extends Component {
    render() {
        return (
            <div className="section-homepage">
                <div className="container-fluid homepage__container">
                    <div className="homepage__header layout-header d-none d-lg-block">
                        <div className="homepage__header-container layout-header__container">
                            <div className="homepage__header-top layout-header__top">
                                <div className="d-flex align-items-center hp-header-top__container lh-top__container">
                                    <div className="ml-auto hp-header-top__menu lh-top__menu">
                                        <MenuItem />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="homepage-header_mobile d-lg-none">
                        <div className="header_mobile__container">
                            <div className="header_mobile__content d-flex justify-content-between">
                                <div className="menu_mobile__drawer">
                                    <div className="mobile-drawer__content d-flex align-items-center">
                                        <div className="drawer-toggler" data-toggle="modal" data-target="#drawerToggleExternalContent" aria-controls="drawerToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation"><img src="../../assets/img/icon-drawer.png" alt="" /></div>
                                        <div className="modal fadeInRight" id="drawerToggleExternalContent" role="dialog" aria-labelledby="drawerToggleExternalContent" aria-hidden="true">
                                            <div className="modal-content">
                                                <nav className="mobile-navbar">
                                                    <div className="mobile-navbar__content"><a className="navbar-brand" href="/"><img src="../../assets/img/asset-logo.png" alt="" /></a>
                                                        <NavbarItem/>
                                                    </div>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <MenuMobile/>
                            </div>
                        </div>
                    </div>
                    <div className="homepage-body">
                        <HomePageBody/>
                    </div>
                    <div className="homepage__footer layout-footer">
                        <div className="homepage__footer-container">
                            <MenuFooter/>
                            {/* <!-- Desktop Display--> */}
                            {/* <Footer/> */}
                            {/* <!-- Mobile Display--> */}
                            <FooterMobile/>
                            {/* <NewsDetail/> */}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

