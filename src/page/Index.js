import React, { Component } from 'react'
import '../vendor/ap8/css/style.css'
import '../vendor/font-awesome/css/font-awesome.min.css'
import '../vendor/bootstrap/css/bootstrap.min.css'
import '../assets/stylesheets/index.css'
import '../assets/stylesheets/pages/_resultpages.css'
import '../assets/stylesheets/partials/map/filter-bar.css'
import '../assets/stylesheets/partials/map/bootstrap-tagsinput.css'
import '../assets/stylesheets/partials/chat/index.css'
import Header from '../components/Index/Header'
import SearchResult from '../components/Index/SearchResult'
import { connect } from 'react-redux'
import NewsDetail from './NewsDetail'
import Pagination from '../Pagination/Pagination'
import queryString from 'query-string'
import { withRouter } from 'react-router-dom';
import {actFetchPostByKeywordRequest,actCountByKeywordRequest} from '../actions/index'
import Error from '../components/Index/Error'
class Index extends Component {
    constructor(props) {
        super(props)

        this.state = {
            listPost: '',
        }
    }
    componentDidMount(){
        const {location } = this.props;
        let url = location.search
        const parsed = queryString.parse(url);
        this.props.fetchPostRequest(parsed.keyword,parsed.page)
        this.props.countByKeywordRequest(parsed.keyword)
    }
    componentWillReceiveProps(e) {
        if (e.fetchPost.length !== 0) {
            this.setState({
                listPost: e.fetchPost
            })
        } else {
            this.setState({
                listPost: ''
            })
        }
    }
    renderPagination=()=>{
        if(this.props.sl!==0){
            return <Pagination/>
        }
        else{
            return <Error/>
        }
    }
    render() {
        let listItem=[]
        if (this.state.listPost !== '') {
            listItem = this.state.listPost.map((item, index) => (
                <SearchResult
                    key={index}
                    item={item}
                    isDisplayModal = {this.openModal}
                    loadDay={this.loadDay}
                />
            )   
            )
        }
        return (
            <section className="section__result-pages">
                <div className="container-fluid result-pages__container">
                    <div className="result-pages__header layout-header">
                        <Header length={this.state.listPost.length}/>
                    </div>
                    <div className="result-pages__body">
                        <div className="result-pages__body-container container">
                            <div className="result-pages__search-result">
                                <div className="rp-search-result__header">
                                    <div className="text-result">Khoảng <strong>{this.props.sl}</strong> kết quả</div>
                                </div>
                                <div className="rp-search-result__items">
                                    {/* <SearchResult /> */}
                                    {
                                        listItem
                                    }
                                </div>
                                {this.renderPagination()}
                                <NewsDetail />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        fetchPost: state.News,
        sl:state.Keyword
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchPostRequest: (keyword,page) => {
            dispatch(actFetchPostByKeywordRequest(keyword, page))
        },
        countByKeywordRequest:keyword=>{
            dispatch(actCountByKeywordRequest(keyword))
        }
    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Index))