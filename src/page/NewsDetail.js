import React, { Component } from 'react'
import '../../src/assets/stylesheets/pages/_new-detail-popup.css'
import '../assets/stylesheets/style.css'
import { connect } from 'react-redux'
import ReactHtmlParser from 'react-html-parser';

class NewsDetail extends Component {
    constructor(props) {
        super(props)

        this.state = {
            avatar: '',
            first_name: '',
            last_name: '',
            description: '',
            name: '',
            content: '',
            image_url: '',
            day: "",
            flash:" "
        }
    }
    componentWillReceiveProps(e) {
        if (typeof e.Detail[0] === 'object') {
            let date_publish = e.Detail[0].publish_date

            let one_day = 1000 * 60 * 60 * 24
            let present_date = new Date()
            let public_date = new Date(present_date.getFullYear(), date_publish.substr(5, 2) - 1, date_publish.substr(8, 2))
            let Result = Math.round(present_date.getTime() - public_date.getTime()) / (one_day)
            let Final_Result = Result.toFixed(0);

            if (Final_Result / 30 >= 1) {
                let a = Final_Result / 30
                let b = a.toFixed(0)
                this.setState({ day: b + " tháng trước" })
            } else (
                this.setState({ day: Final_Result + " ngày trước" })
            )
            this.setState({
                avatar: e.Detail[0].create_uid.avatar,
                first_name: e.Detail[0].create_uid.first_name,
                last_name: e.Detail[0].create_uid.last_name,
                description: e.Detail[0].description,
                name: e.Detail[0].name,
                content: e.Detail[0].content,
                image_url: e.Detail[0].image_url,
                create_user_role: e.Detail[0].create_user_role,
                flash: e.Detail[0].flash
            })
        }
    }
    render() {
        return (
            <section className="News-Detail-Page">
                <div className="modal news-details__modal" id="newsDetailsModal">
                    <div className="ndt-modal__container">
                        <div className="modal-content">
                            {/* Modal Header */}
                            <div className="modal-header" style={{ padding: "20px" }}>
                                <button type="button" className="close close-button" data-dismiss="modal" onClick={this.onClick}>×</button>
                                <div className="clearfix" />
                            </div>
                            {/* Modal body */}
                            <div className="modal-body">
                                <div className="modal-body__container container">
                                    <div className="news-details__content-top">
                                        <div className="news-details__header d-flex">
                                            <div className="nd-header__logo">
                                                <img className="nd-header--logo" src={this.state.avatar} width="60px" height="60px" />
                                            </div>
                                            <div className="nd-header__title">
                                                <div className="header-title__container">
                                                    <a href="#" className="header-title-name">{this.state.first_name + ' ' + this.state.last_name}</a>
                                                    <div className="header-title__content">[
                                                        <span className="small-text">{this.state.create_user_role}</span>
                                                        ]</div>
                                                    <span className="header-title--time-post">{this.state.day}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="nd-header__title-text">{this.state.name}</div>
                                        {/* <img className="edit-picture" src={this.state.image_url} /> */}
                                        <div className="nd-content__texts">
                                            {ReactHtmlParser(this.state.content)}
                                        </div>
                                    </div>
                                    <div className="info-fix">
                                        <div className="name">{this.state.first_name + ' ' + this.state.last_name}</div>
                                        <div className="post-time">{this.state.day}</div>
                                        <a className="btn btn-follow" href="#">Theo giõi</a>
                                        <div className="actions">
                                            <div>
                                                <a href="#" className="btn">
                                                    <span className="-ap  icon-like2 icon" /> {this.state.flash}
                                                </a>
                                            </div>
                                            <div>
                                                <a href="#" className="btn">
                                                    <span className="-ap  icon-share4 icon" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    {/*--------- Content-bottom -------*/}
                                    <div className="divider-gray" />
                                    <div className="news-details__content-bottom">
                                        <div className="box-comments" style={{ paddingTop: '20px' }}>
                                            <h2 className="title-main">Bình luận</h2>
                                            <div className="comments">
                                                <div className="item">
                                                    <div className="avatar" style={{ backgroundImage: 'url(https://www.bitgab.com/uploads/profile/files/default.png)' }}>
                                                    </div>
                                                    <div className="username">Lâm Thy Văn Tần <span className="datetime">12:09 - 18/10/2019</span></div>
                                                    <div className="comment-content">Đầu tư an toàn, bảo toàn dòng vốn. Cam kết lợi nhuận 10%/năm. Hỗ trợ lãi suất
                                                      0%/tháng. Bảo đảm chất lượng với đơn vị vận hành quốc tế. Giá trị bất động sản tăng theo hàng năm. Cho vay
                            lên tới 65% Cơ hội du lịch miễn phí. Vốn đầu tư từ 600 triệu. Tặng 15 đêm nghỉ dưỡng.</div>
                                                    <div className="action-buttons-bottom">
                                                        <a href="#" className="btn">
                                                            Trả lời
                                                        </a>
                                                        <a href="#" className="btn -comment">
                                                            <span className="fa fa-comment-o icon" />
                                                        </a>
                                                        <a href="#" className="btn">
                                                            <span className="-ap  icon-like2 icon" />
                                                        </a>
                                                        <a href="#" className="btn -share">
                                                            <span className="-ap  icon-share4 icon" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="divider-gray">
                                    </div>
                                    <div className="bottom-related-news">
                                        <div className="title-main">Các bài viết liên quan</div>
                                        <div className="list-news">
                                            <div className="item">
                                                <div className="wrap">
                                                    <a href="#" className="post-image" style={{ backgroundImage: 'url(https://cdn.tuoitre.vn/thumb_w/640/2019/1/16/photo-1-15476236955311643255083.jpg)' }}>
                                                        <img src="../../assets/img/204x102.jpg" alt="" />
                                                    </a>
                                                    <h3 className="post-title">
                                                        <a href="#">Tòa nhà Lý Chính Thắng đang trong giai đoạn hoàn thiện…</a>
                                                    </h3>
                                                    <div className="post-time">
                                                        12:09 - 18/10/2019
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        Detail: state.Detail
    }
}
export default connect(mapStateToProps, null)(NewsDetail);