import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Router from './routers/Router';
import * as serviceWorker from './serviceWorker';
import thunk from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux'
import myReducer from './reducers/index'
import { Provider } from 'react-redux'
const store = createStore(myReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(thunk)
)
ReactDOM.render(
    <Provider store={store}>
        <Router />
    </Provider>,
    document.getElementById('root'));
serviceWorker.unregister();
