import * as types from '../constants/ActionType'
var initialState = []
var myNews = (state = initialState, action) => {
    switch (action.type) {
        case types.FETCH_POST_BY_KEYWORD_SUCCESS:
            const { data } = action.payload
            return data
        default: return state
    }
}
export default myNews