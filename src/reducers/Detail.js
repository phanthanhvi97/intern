import * as types from '../constants/ActionType'
var initialState = []
var myReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_POST_BY_ID_SUCCESS:
            const { data } = action.payload
            return data
        default: return state
    }
}
export default myReducer