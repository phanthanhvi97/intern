import * as types from '../constants/ActionType'
var initialState = {
}
var mySuggest = (state = initialState, action) => {
        switch (action.type) {
                case types.FETCH_SUGGEST_SUCCESS:{
                        const {data} = action.payload
                        return data
                }
                case types.FETCH_SUGGEST_FAIL:
                        const {err} = action.payload
                        alert("error")
                        return err
                default: return state
        }
}
export default mySuggest 