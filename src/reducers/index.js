import {combineReducers} from 'redux'
import Searching from './Searching'
import News from './News'
import Keyword from './Keyword'
import Detail from './Detail'
const myReducer = combineReducers({
    Searching,
    News,
    Keyword,
    Detail
})
export default myReducer